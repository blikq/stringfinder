## MY REPORT
The time of searching for a path in the config file ranging from 10,000-1,000,000 rows and the time taken for each query in the task completed line. The result is as follow:

    10000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010489702224731445 seconds
    Timestamp is: 1671922043.681255
    
    40000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010334253311157227 seconds
    Timestamp is: 1671922044.63144
    
    70000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010189294815063477 seconds
    Timestamp is: 1671922046.172222
    
    100000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.009983539581298828 seconds
    Timestamp is: 1671922047.332531
    
    130000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010334968566894531 seconds
    Timestamp is: 1671922048.721129
    
    160000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010335445404052734 seconds
    Timestamp is: 1671922050.262333
    
    190000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010505199432373047 seconds
    Timestamp is: 1671922052.859878
    
    220000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010658502578735352 seconds
    Timestamp is: 1671922054.450814
    
    250000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010201692581176758 seconds
    Timestamp is: 1671922056.215567
    
    280000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010141372680664062 seconds
    Timestamp is: 1671922057.891929
    
    310000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010181665420532227 seconds
    Timestamp is: 1671922060.470195
    
    340000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010436534881591797 seconds
    Timestamp is: 1671922062.637719
    
    370000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010233163833618164 seconds
    Timestamp is: 1671922064.661015
    
    400000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010427713394165039 seconds
    Timestamp is: 1671922066.906418
    
    430000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010227680206298828 seconds
    Timestamp is: 1671922068.961347
    
    460000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.01045846939086914 seconds
    Timestamp is: 1671922071.171276
    
    490000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010258674621582031 seconds
    Timestamp is: 1671922073.371469
    
    520000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010438919067382812 seconds
    Timestamp is: 1671922075.441411
    
    550000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010213613510131836 seconds
    Timestamp is: 1671922077.76949
    
    580000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010152101516723633 seconds
    Timestamp is: 1671922079.811111
    
    610000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.01075434684753418 seconds
    Timestamp is: 1671922081.811741
    
    640000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010393381118774414 seconds
    Timestamp is: 1671922084.241199
    
    670000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.01032257080078125 seconds
    Timestamp is: 1671922086.37033
    
    700000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010871648788452148 seconds
    Timestamp is: 1671922099.800903
    
    730000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010268449783325195 seconds
    Timestamp is: 1671922116.288656
    
    760000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010309696197509766 seconds
    Timestamp is: 1671922127.963986
    
    790000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010192155838012695 seconds
    Timestamp is: 1671922134.342848
    
    820000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.01021718978881836 seconds
    Timestamp is: 1671922139.291066
    
    850000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010249853134155273 seconds
    Timestamp is: 1671922142.621714
    
    880000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010383129119873047 seconds
    Timestamp is: 1671922145.92121
    
    910000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010665416717529297 seconds
    Timestamp is: 1671922148.85156
    
    940000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.01058340072631836 seconds
    Timestamp is: 1671922151.752429
    
    970000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010337114334106445 seconds
    Timestamp is: 1671922154.401264
    
    1000000 rows
    STRING NOT FOUND
    DEBUG:
    Query String: 13;0;23;11;0;16;5;0;   
    Request From IP: ('127.0.0.1', 57984)
    task completed in 0.010493755340576172 seconds
    Timestamp is: 1671922157.139866

