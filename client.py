###THIS CLIENT FILE IS ONLY FOR TESTING

import socket

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.connect(('localhost', 8090))
request = None

try:
    while request != 'quit':
        request = str(input('>> '))
        if request:
            server.send(request.encode('utf8'))
            response = server.recv(255).decode('utf8')
            print(response)
except KeyboardInterrupt:
    server.close()