## ABOUT :

This code is a simple server that listens for connections on port 8090 on the local machine. When a client connects, the server reads in their request and processes it by calling the read_config function.

The read_config function reads in a file called "config.txt" and looks for lines that start with the prefix "linuxpath=". If it finds such a line, it removes the prefix and any whitespace, and uses the resulting string as the file path of a file to open. It then searches the contents of this file for a specified query string. If the query string is found, it updates a dictionary called is_true with the value "STRING EXISTS". If the query string is not found, the function does nothing. If any errors occur while opening or reading the file, they are caught and logged (though this feature is currently commented out).

After the read_config function has been called, the handle_client function sends a response back to the client based on the value of the is_true dictionary. If the query string was found in the file specified in the config file, the response is "STRING EXISTS". If the query string was not found or an error occurred, the response is "STRING NOT FOUND". If the request was of an invalid type, the response is "Invalid Request Type".

The server runs in a loop, waiting for and accepting incoming connections. When a connection is accepted, a new thread is spawned to handle the client's request, and the main thread waits for the next connection.

  
  

## INSTALLATION :

1.  Install python 3.7 or >
    
2.  No packages need to be installed due to all required packages being already in python’s standard library
    
3.  Clone this gitlab repository to your working directory https://gitlab.com/blikq/stringfinder.git by running

    git clone https://gitlab.com/blikq/stringfinder.git
4. Create a config.txt file in your working directory with the paths you want to check for starting with "linuxpath=", if not done, server will not start.
5. After doing that, you simply run the server.py file
    python3 server.py
6. The server should be running fine on port 8090, if the port is free, if the port is not free go into the server.py file and change the port variable to any free port number
7. You should be able to hit the socket from your client, all errors are logged to log.txt file that is later generated.

This is the tcp link to a server that is already working <tcp://6.tcp.ngrok.io:11604>. feel fre to test it with strings from the is 200k.txt file <https://www.dropbox.com/s/vx9bvgx3scl5qn4/200k.txt?dl=0.>
