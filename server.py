# Import the threading and socket modules
import threading
import socket
import logging
import time
import datetime

PORT = 8090

logging.basicConfig(filename="log.txt")
level = logging.INFO

# Define a function to read a config file and search for a query string


def read_config(query: str, is_true: dict):
    """
    This function reads a configuration file called "config.txt" and searches for a given query string in a file specified in the config file. If the query string is found in the file, the is_true dictionary is updated with the value "STRING EXISTS". If the query string is not found or if an error occurs while reading the file, the is_true dictionary is updated with the value "STRING NOT FOUND".

    Parameters:
    query (str): The query string.
    is_true (dict): The dictionary that shows response.

    Returns:
    None
    """
    # Open the config file for reading, with utf-8 encoding
    with open("config.txt", 'r', encoding="utf-8") as config_file:
        # Read each line of the config file
        for line in config_file:
            # If the line starts with the prefix "linuxpath=", process it
            if line.strip().startswith("linuxpath="):
                # Remove the prefix "linuxpath=" and strip any whitespace
                file_path = line.removeprefix('linuxpath=').strip()
                # Open the file specified in the config line and check if the query appears in it
                try:
                    with open(file_path, 'r') as query_file:
                        if query_file.read().find(query) != -1:
                            # If the query string is found, update the is_true dictionary and return
                            return is_true.update({"is_true": "STRING EXISTS"})
                        else:
                            # If the query string is not found, do nothing
                            pass
                except Exception as e:
                    # If an error occurs while opening or reading the file, log the exception
                    logging.exception(e)
                    pass
    # If the query string is not found and no errors occurred, update the is_true dictionary with "STRING NOT FOUND"
    return is_true.update({"is_true": "STRING NOT FOUND"})

# Define a function to handle a client's request


def handle_client(client_socket, ip_addr):
    """
    This function is called when the server receives a connection from a client. It reads a request from the client (up to 1024 bytes), processes the request by calling the read_config function, and sends a response back to the client. The response is either the value in the is_true dictionary (either "STRING EXISTS" or "STRING NOT FOUND").

    Parameters:
    client_socket (str): object from client to obtain the request from.
    ip_addr (tuple): IP address of the client

    Returns:
    None
    """
    # Keep processing the client's request until it closes the connection
    while True:
        # Read the request from the client (up to 1024 bytes)
        try:
            request = client_socket.recv(1024)
            st_time = time.time()

            # Initialize the is_true dictionary
            is_true = {"is_true": False}
            # Call the read_config function to process the request
            read_config(str(request.decode('utf8')), is_true)
            time_ = f"task completed in {time.time() - st_time} seconds"
            # Set the response to the value in the is_true dictionary
            response = f'{is_true["is_true"]}\nDEBUG:\nQuery String: {str(request.decode("utf8"))}\nRequest From IP: {ip_addr}\n{time_}\nTimestamp is: {datetime.datetime.timestamp(datetime.datetime.utcnow())}\n'
            # Send the response back to the client
            client_socket.send(response.encode('utf8'))
            # Close the client's socket
        except:
            break
    client_socket.close()


try:
    # Create a socket for the server
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind the socket to localhost:8090
    server_socket.bind(("localhost", PORT))
    # Listen for up to 5 incoming connections
    server_socket.listen(5)

    # Run the server indefinitely

    while True:
        # Accept an incoming connection
        client_socket, client_address = server_socket.accept()
        # print(f"Received connection from {client_address}")
        client_thread = threading.Thread(
            target=handle_client, args=(client_socket, client_address,))
        client_thread.start()

except:
    server_socket.close()
